import pandas as pd
import argparse
import sqlite3


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('-a', '--acm', required=False, help='path to acm file')
    ap.add_argument('-i', '--ieee', required=False, help='path to ieee file')
    ap.add_argument('-s', '--science', required=False, help='path to science direct folder')
    ap.add_argument('-c', '--scopus', required=False, help='path to scopus folder')
    ap.add_argument('-p', '--springer', required=False, help='path to springer folder')
    args = vars(ap.parse_args())

    if args['acm'] is not None:
        acm_file = pd.read_csv(args['acm'])
    if args['ieee'] is not None:
        ieee_file = pd.read_csv(args['ieee'])
    if args['science'] is not None:
        science_file = pd.read_csv(args['science'])
    if args['scopus'] is not None:
        scopus_file = pd.read_csv(args['scopus'])
    if args['springer'] is not None:
        springer_file = pd.read_csv(args['springer'])

    acm_file['author'] = acm_file['author'].str.replace(' and ', ';')
    print(acm_file['author'])
    #print(ieee_file['Authors'])
    #print(ieee_file.head())

    # conn = sqlite3.connect('review.db')
    # cursor = conn.cursor()
    # cursor.execute("""
    # CREATE TABLE IF NOT EXISTS article(
    #     id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    #     base VARCHAR(10) NOT NULL,
    #     title TEXT NOT NULL,
    #     author TEXT NOT NULL,
    #     doi TEXT NOT NULL,
    #     year INTERGER NOT NULL,
    #     abstract TEXT);
    #     """)


    # # print(acm_file["title"])
    # # for article in acm_file:
    # #     print(article[])

    # cursor.execute("""
    #     INSERT INTO articles(base, title, author, doi, year, abstract)
    #     VALUES ('ACM', )""")

    # conn.close()


    # nome_arquivo_json = args['project']


if __name__ == '__main__':
    main()
