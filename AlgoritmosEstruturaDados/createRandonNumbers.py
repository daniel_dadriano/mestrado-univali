import numpy as np
import csv

def save_numbers(numbers, file_name):
	f = open(file_name, "w")
	for n in numbers:
		f.write("{0},".format(int(n)))
	f.close()


def generate(seed, max_seed):
	random_numbers = np.arange(1, max_seed + 1)
	numbers = np.zeros(seed)

	for i in range(seed):
		n = np.random.choice(random_numbers)
		numbers[i] = n
		random_numbers = np.delete(random_numbers, np.where(random_numbers==n))

	file_name = "{0}_shuffle.txt".format(seed)
	save_numbers(numbers, file_name)
	
	numbers.sort()
	file_name = "{0}_cres.txt".format(seed)
	save_numbers(numbers, file_name)

	numbers = numbers[::-1]
	file_name = "{0}_dec.txt".format(seed)
	save_numbers(numbers, file_name)

generate(500, 1000)
generate(5000, 10000)
generate(10000, 20000)
generate(30000, 60000)
