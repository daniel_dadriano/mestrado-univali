import argparse
import numpy as np
import time


def load_file(arg):
	"""load file"""
	try:
		f = open(arg, 'r')
		numbers_aux = f.read().split(',')
		numbers = np.zeros(len(numbers_aux))
		for i, n in enumerate(numbers_aux):
			numbers[i] = n

		return numbers
	except Exception as e:
		raise "Load file error: {0}".format(e)
	finally:
		f.close


def insertion_sort(numbers):
	seed = len(numbers)
	start_time = time.time()
	for i in range(1, seed):
		key = numbers[i]
		k = i
		while k > 0 and key < numbers[k - 1]:
			numbers[k] = numbers[k - 1]
			k -= 1
		numbers[k] = key
	end_time = time.time()

	print("Insertion sort - Process time: {0}".format(end_time - start_time))


def selection_sort(numbers):
	seed = len(numbers)
	start_time = time.time()
	for i in range(0, seed):
		key = i
		for j in range(i + 1, seed):
			if numbers[j] < numbers[key]:
				key = j
		if key != i:
			aux = numbers[i]
			numbers[i] = numbers[key]
			numbers[key] = aux
	end_time = time.time()

	print("Selection sort - Process time: {0}".format(end_time - start_time))


def shell_sort(numbers):

	start_time = time.time()
	h = 0

	aux = (len(numbers) / 3)
	while h < aux:
		h = h * 3 +1

	h_aux = []
	while h > 0:
		h_aux.append(h)		
		
		for i in range(h, len(numbers)):
			value = numbers[i]
			inner = i
			while inner > h - 1 and numbers[inner - h] >= value:
				numbers[inner] = numbers[inner - h]
				inner = inner - h

			numbers[inner] = value

		h = int((h - 1) / 3)

	end_time = time.time()

	print("Shell sort - Process time: {0}".format(end_time - start_time))
	print("Shell sort - inteval (h) {0}".format(h_aux))


ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", required=True, help="path to the input file")
args = vars(ap.parse_args())

numbers = load_file(args["file"])
insertion_sort(numbers)
selection_sort(numbers)
shell_sort(numbers)
