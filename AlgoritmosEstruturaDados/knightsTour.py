import numpy as np
import random
import argparse
import time
from anytree import Node, RenderTree
# pylint: disable=W0312

n = 8
board = []
xMove = np.array([2, 1, -1, -2, -2, -1, 1, 2])
yMove = np.array([1, 2, 2, 1, -1, -2, -2, -1])


def set_board():
	global board
	board = [[0 for i in range(n)] for j in range(n)]
	board = np.array(board)


def legal_move(x, y):
	return x < n and y < n and x >= 0 and y >= 0 and board[x][y] == 0


def degree(x, y):
	deg = 0
	for i in range(8):
		next_x = x + xMove[i]
		next_y = y + yMove[i]
		if legal_move(next_x, next_y):
			deg += 1
	return deg


def get_moves(x, y):
	next_move = []
	aux_degree = 8
	for i in range(8):
		next_x = x + xMove[i]
		next_y = y + yMove[i]

		if legal_move(next_x, next_y):
			deg = degree(next_x, next_y)
			if deg < aux_degree:
				aux_degree = deg
				next_move = []
				next_move.append([next_x, next_y])
			elif deg == aux_degree:
				next_move.append([next_x, next_y])
	return next_move

def warnsdorff(x, y, num_moves, start_time):
	path = [[-1 for i in range(2)] for j in range(n*n)]
	path = np.array(path)
	path[num_moves - 1] = x, y

	ok = False
	while not ok:
		next_move = get_moves(x, y)

		if len(next_move) > 0:
			next_x, next_y = random.choice(next_move)
			num_moves += 1
			board[next_x, next_y] = num_moves
			path[num_moves - 1] = next_x, next_y
			if num_moves == 64:
				ok = True
			else:
				x, y = next_x, next_y
		else:
			x, y = path[num_moves - 1]
			board[x, y] = -1
			path[num_moves - 1] = -1, -1
			num_moves -= 1

	end_time = time.time()
	print(board)
	print("Process time: {0}".format(end_time - start_time))


def all_solutions(x, y, num_moves, start_time):
	if num_moves == 64:
		end_time = time.time()
		print("Process time: {0}".format(end_time - start_time))
		print(num_moves)
		print(board)
		return True

	for i in range(n):
		next_x = x + xMove[i]
		next_y = y + yMove[i]

		if legal_move(next_x, next_y):
			board[next_x, next_y] = num_moves + 1
			all_solutions(next_x, next_y, num_moves + 1, start_time)
			board[next_x, next_y] = 0


def main(x, y, a):
	set_board()
	board[x, y] = 1
	start_time = time.time()

	if a == "w":
		warnsdorff(x, y, 1, start_time)
	else:
		all_solutions(x, y, 1, start_time)


ap = argparse.ArgumentParser()
ap.add_argument("-x", "--x", required=True, help="Pos X")
ap.add_argument("-y", "--y", required=True, help="Pos Y")
ap.add_argument("-a", "--a", required=True, help="w -> warnsdorff a -> all solutions")
args = vars(ap.parse_args())

main(int(args["x"]), int(args["y"]), args["a"])
