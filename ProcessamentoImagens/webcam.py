import cv2
import numpy as np

cap = cv2.VideoCapture(1)
# "bf1.mp4"

while(True):
    ret, frame = cap.read()

    roi = frame[0:350, 0:350]

    gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    #gray = cv2.medianBlur(gray, 1)
    edges = cv2.Canny(gray, 50, 100)

    cv2.rectangle(frame, (0,0), (350,350), (0,255,0), 3)

    cv2.imshow('WebCam', frame)
    cv2.imshow('roi', edges)
    if cv2.waitKey(1) &  0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
