import numpy as np
import cv2


def get_gray(image):
    return int((int(bgr[0]) + int(bgr[1]) + int(bgr[2])) / 3)


def get_weighted_gray(bgr):
    return bgr[0] * 0.114 + bgr[1] * 0.587 + bgr[2] * 0.299


def get_negative(bgr):
    return [255 - bgr[0], 255 - bgr[1], 255 - bgr[2]]


def get_threshold(pixel, threshold):
    if pixel > threshold:
        return 255
    else:
        return 0


def get_convolution():
    kernel = np.array([[-1, -1, -1],
                       [-1, 8, -1],
                       [-1, -1, -1]])

    soma = kernel[0][0] * 



image = cv2.imread('/home/daniel/projetos/univali/ProcessamentoImagens/flower.jpg')

height, width, _ = image.shape

negative_image = np.zeros(image.shape, np.uint8)
gray_image = np.zeros((height, width), np.uint8)
w_gray_image = np.zeros((height, width), np.uint8)
threshold_image = np.zeros((height, width), np.uint8)

for i in range(height):
    for j in range(width):
        bgr = image[i, j]

        gray_image[i, j] = get_gray(bgr)
        w_gray_image[i, j] = get_weighted_gray(bgr)
        negative_image[i, j] = get_negative(bgr)
        threshold_image[i, j] = get_threshold(gray_image[i, j], 100)
        

cv2.imshow('Gray', gray_image)
cv2.imshow('Weighted Gray', w_gray_image)
cv2.imshow('Nagative', negative_image)
cv2.imshow('Threshold', threshold_image)
cv2.imshow('Image', image)
cv2.waitKey(0)