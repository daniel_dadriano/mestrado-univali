import io
import requests
import sys
import os
import csv
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn import preprocessing
from sklearn.metrics import matthews_corrcoef
from scipy.stats import pearsonr
import sqlite3
from datetime import datetime
import numpy as np


def get_paths(root_path):
    """Return the video paths"""
    root = [root for root, subdirs, files in os.walk(root_path)]
    files = [files for root, subdirs, files in os.walk(root_path)]
    paths = []
    for i, r in enumerate(root):
        for f in files[i]:
            if len(f) > 0 and f.find('.csv') > -1:
                paths.append([os.path.join(r, f), f[:f.find('_')]])
    return paths


#Date	Open	High	Low	Close	Volume	Market Cap
def create_table(conn):
    cursor = conn.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS price(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    type VARCHAR(10),
    date DATETIME,
    open FLOAT,
    high FLOAT,
    low FLOAT,
    close FLOAT,
    volume FLOAT,
    market_cap FLOAT);
    """)


def get_price_data(file_path, crypto_coin):
    crypto = pd.read_csv(file_path, memory_map=True)
    tmp = []
    for index, row in crypto.iterrows():
        type = crypto_coin
        date = pd.to_datetime(row['Date'])
        date = datetime.date(date)
        open = float(row['Open'])
        high = float(row['High'])
        low = float(row['Low'])
        close = float(row['Close'])
        try:
            volume = float(row['Volume'].replace(',', ''))
        except ValueError as identifier:
            volume = 0
        try:
            market_cap = float(row['Market Cap'].replace(',', ''))
        except ValueError as identifier:
            market_cap = 0

        tmp.append([type, date, open, high, low, close, volume, market_cap])
    return tmp


def insert(conn, data):
    cursor = conn.cursor()
    try:
        cursor.executemany("""
        INSERT INTO price(type, date, open, high, low, close, volume, market_cap)
        VALUES(?,?,?,?,?,?,?,?)
        """, data)
        conn.commit()
    except Exception as ex:
        print(ex)


def import_price_data(conn, paths):
    for path in paths:
        print(path)
        prices = get_price_data(path[0], path[1])
        insert(conn, prices)


def get_prices(conn, type):
    prices = pd.read_sql_query("SELECT * FROM price WHERE type = '"+type+"' ORDER BY date;", conn)
    prices['date'] = pd.to_datetime(prices['date'])
    return prices


def get_bitcoin(conn):
    crypto = pd.read_sql_query("SELECT * FROM bitcoin ORDER BY date;", conn)
    crypto['date'] = pd.to_datetime(crypto['date'])
    return crypto
    

#paths = get_paths("./prices")
conn = sqlite3.connect("crypto.db")

#import_price_data(conn, paths)
bitcoin = get_bitcoin(conn)
prices = get_prices(conn, 'bitcoin')
conn.close()

bitcoin = pd.merge(bitcoin, prices, how='right', on='date')
del bitcoin["market_cap_y"]
del bitcoin["id"]
bitcoin = bitcoin.rename(columns = {"market_cap_x": "market_cap"})

# normalized_df = (bitcoin["difficulty"] - bitcoin["difficulty"].min())/(bitcoin["difficulty"].max()-bitcoin["difficulty"].min())
# print(normalized_df)

m_dif = bitcoin["difficulty"].max()
bitcoin["difficulty"] = bitcoin["difficulty"] / m_dif

m_open = bitcoin["open"].max()
bitcoin["open"] = bitcoin["open"] / m_open

m_high = bitcoin["high"].max()
bitcoin["high"] = bitcoin["high"] / m_high

m_low = bitcoin["low"].max()
bitcoin["low"] = bitcoin["low"] / m_low

m_close = bitcoin["close"].max()
bitcoin["close"] = bitcoin["close"] / m_close

m_market = bitcoin["market_price"].max()
bitcoin["market_price"] = bitcoin["market_price"] / m_market

m_total = bitcoin["total_bitcoins"].max()
bitcoin["total_bitcoins"] = bitcoin["total_bitcoins"] / m_total

m_trade = bitcoin["trade_volume"].max()
bitcoin["trade_volume"] = bitcoin["trade_volume"] / m_trade

market_price = bitcoin["market_price"].values
difficulty = bitcoin["difficulty"].values
open = bitcoin["open"].values
low = bitcoin["low"].values
high = bitcoin["high"].values
close = bitcoin["close"].values
total_bitcoins = bitcoin["total_bitcoins"].values
trade_volume = bitcoin["trade_volume"].values

#print(np.corrcoef(market_price, difficulty))
#print(matthews_corrcoef(market_cap, difficulty))

print("difficulty: {0}".format(pearsonr(market_price, difficulty)))
print("open: {0}".format(pearsonr(market_price, open)))
print("low: {0}".format(pearsonr(market_price, low)))
print("high: {0}".format(pearsonr(market_price, high)))
print("close: {0}".format(pearsonr(market_price, close)))
print("total bitcoin: {0}".format(pearsonr(market_price, total_bitcoins)))
#print("trade volume: {0}".format(pearsonr(market_price, trade_volume)))
print("-----------------")

clf_close = LinearRegression()
clf_market = LinearRegression()

x = np.column_stack((open, low, high))
clf_close.fit(x, close)
predict = np.array((5747.95 / m_open, 5976.80 / m_low, 5721.22 / m_high))
predict = predict.reshape(1, -1)
c = clf_close.predict(predict)
print(c * m_close)

y = np.column_stack((open, low, high, close))
clf_market.fit(y, market_price)
predict = np.array((5747.95 / m_open, 5976.80 / m_low, 5721.22 / m_high, c))
predict = predict.reshape(1, -1)
m = clf_market.predict(predict)
print(m * m_market)


#predict = np.array((5747.95 / m_open, 5976.80 / m_low, 5721.22 / m_high, 5904.83 / m_close))
#predict = np.array((5899.74 / m_open, 5988.39 / m_low, 5728.82 / m_high, 5780.90 / m_close))
#predict = predict.reshape(1, -1)
#market_p = clf_close.predict(predict)
#print(market_p * m_market)

# coef = clf.coef_
# intercept = clf.intercept_
# print(intercept)
# print(coef)


# print("\n open: {0} \n low: {1} \n high: {2} \n close: {3} \n".
#     format(coef[0] * m_open, coef[1] * m_low, coef[2] * m_high, coef[3] * m_close))

# print("\n open: {0} \n low: {1} \n high: {2} \n close: {3} \n".
#     format(bitcoin["open"][1641] * m_open, bitcoin["low"][1641] * m_low,
#         coef[2] * m_high, coef[3] * m_close))


# bitcoin["open"][1641]
# bitcoin["low"]
# bitcoin["high"]
# bitcoin["close"]

#print(matthews_corrcoef(bitcoin["market_cap"].values, bitcoin["difficulty"].values))


# x = bitcoin["difficulty"].values
# x = x.reshape(1, -1)
# min_max_scaler = preprocessing.MinMaxScaler()
# x_scaled = min_max_scaler.fit_transform(x)
# bitcoin["difficulty_n"] = x_scaled[0]
# print(bitcoin["difficulty_n"][1641])
# print(bitcoin["difficulty"][1641])

#print(bitcoin.head(1))
#print(bitcoin["market_cap_y"].head(2))
#bitcoin.to_csv("./bitcoin.csv")


# bitcoin['Date_mpl'] = bitcoin['Date'].apply(lambda x: mdates.date2num(x))
# fig, ax = plt.subplots(figsize=(12, 8))
# sns.tsplot(bitcoin.market_cap.values, time=bitcoin.Date_mpl.values, alpha=0.8, ax=ax)
# ax.xaxis.set_major_locator(mdates.AutoDateLocator())
# ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y.%m.%d'))
# fig.autofmt_xdate()
# plt.xlabel('Date', fontsize=12)
# plt.ylabel('Price in USD', fontsize=12)
# plt.title('Closing price distribution of bitcoin', fontsize=15)
# plt.show()

#print(bitcoin["market_price"])


# print(bitcoin['Market Cap'])

#print(bitcoin.loc[:, 'Market Cap'].replace(',', ''))
#print(bitcoin.loc[:, 'Market Cap'])

#print(bitcoin['Market Cap'])

#for k in bitcoin.keys():
    #print(k)

#print(bitcoin['Market Cap'].replace(',', ''))
#print(bitcoin['Market Cap'])

#print(pd.keys())
# for value in bitcoin['Market Cap']:
#     value = float(value.replace(',', ''))
#     print(value)

#     print(value)
#print('{:}'.format(bitcoin['Market Cap']))

#X = bitcoin.drop('Market Cap', axis=1)

#lm = LinearRegression()
#lm.fit(X, bitcoin['Market Cap'])


# print(bitcoin['Date'])

# group = bitcoin.groupby([bitcoin['Date'].dt.year]).mean()

#print(group.index.values.shape)
#print(group.index.values)

# plt.plot(group.index.values, group['Close'])
# plt.xlabel('Date', fontsize=12)
# plt.ylabel('Price in USD', fontsize=12)
# plt.title('Closing price distribution of bitcoin', fontsize=15)
# plt.show()

#print(bitcoin['Date'].dt.day)

# for bitcoin in bitcoins:
#     #print(int(bitcoin['Volume']))
#     print(bitcoin['Open'])
#     print(bitcoin['High'])
#     print(bitcoin['Low'])
#     print(bitcoin['Close'])
#     input()
