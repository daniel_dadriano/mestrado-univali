import csv
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split

PATH = 'winequality-white.csv'
PATH2 = 'winequality-white.csv'

def load_data_set():

    wines = csv.DictReader(open(PATH, 'r'), delimiter=';')

    quality = []
    classification = []
    for wine in wines:
        quality.append(wine['quality'])
        classification.append([wine['fixed acidity'], wine['volatile acidity'],
                            wine['citric acid'], wine['residual sugar'],
                            wine['chlorides'], wine['free sulfur dioxide'],
                            wine['total sulfur dioxide'], wine['density'],
                            wine['pH'], wine['sulphates'], wine['alcohol']])

    return np.array(quality), np.array(classification)

quality, classification = load_data_set()
test_q = quality[:20]
test_c = classification[:20]

#clf = svm.SVC(kernel='linear', C = 1.0)
clf = svm.SVC(kernel='rbf', C=1.0, gamma='auto')
#clf = svm.SVR(C=1.0, epsilon=0.2)
#clf = svm.SVC(decision_function_shape='ovr')

class_train, class_test, qual_train, qual_test = train_test_split(classification,
                                                                  quality,
                                                                  test_size=0.2)
clf.fit(class_train, qual_train)
accuracy = clf.score(class_test, qual_test)
print(accuracy)

# clf.fit(classification[20::], quality[20::])

# for i in range(len(test_c)):
#     wine = test_c[i]
#     wine = wine.reshape(1, -1)
#     predict = clf.predict(wine)
#     print('Qualidade: {0} - Qualidade Classificador: {1}'.format(test_q[i], predict))
