import numpy
import pandas as pd
import csv
import pymongo


def isnumber(value):
    try:
        float(value)
    except ValueError:
        return False
    return True


if __name__ == '__main__':
	print('Read csv file')
	file_path = './infopen.csv'

	csvfile = open(file_path, "r")
	reader = csv.DictReader(csvfile, delimiter=';')
	mongo_client = pymongo.MongoClient("localhost", 27017)
	db = mongo_client["infopen"]
	
	header = ["unidade", "Nome da unidade prisional", "Endereço da Unidade", "CEP", "Cidade", "UF"]
	new_header = ["unidade", "nome", "endereco", "cep", "cidade", "uf"]

	header_capacidade = [
		"1.3. Capacidade do estabelecimento | vagas - presos provisórios | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - presos provisórios | Feminino",
		"1.3. Capacidade do estabelecimento | vagas - regime fechado | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - regime fechado | Feminino",
		"1.3. Capacidade do estabelecimento | vagas - regime semiaberto | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - regime semiaberto | Feminino",
		"1.3. Capacidade do estabelecimento | vagas - regime aberto | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - regime aberto | Feminino",
		"1.3. Capacidade do estabelecimento | vagas - Regime Disciplinar Diferenciado (RDD) | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - Regime Disciplinar Diferenciado (RDD) | Feminino",
		"1.3. Capacidade do estabelecimento | vagas - Medidas de segurança de internação | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - Medidas de segurança de internação | Feminino",
		"1.3. Capacidade do estabelecimento | vagas - Outro(s). Qual(is)? (especificar abaixo) | Masculino",
		"1.3. Capacidade do estabelecimento | vagas - Outro(s). Qual(is)? (especificar abaixo) | Feminino"
	]

	header_populacao = [
		"[Q_4_1.0.0] 4.1. População prisional | Presos provisórios (sem condenação)** | Justiça Estadual Masculino",
		"[Q_4_1.1.0] 4.1. População prisional | Presos provisórios (sem condenação)** | Justiça Estadual Feminino",
		"[Q_4_1.2.0] 4.1. População prisional | Presos provisórios (sem condenação)** | Justiça Federal Masculino",
		"[Q_4_1.3.0] 4.1. População prisional | Presos provisórios (sem condenação)** | Justiça Federal Feminino",
		"[Q_4_1.4.0] 4.1. População prisional | Presos provisórios (sem condenação)** | Outros  (Justiça do Trabalho, Cível) Masculino",
		"[Q_4_1.5.0] 4.1. População prisional | Presos provisórios (sem condenação)** | Outros  (Justiça do Trabalho, Cível) Feminino",
		"[Q_4_1.0.1] 4.1. População prisional | Presos sentenciados - regime fechado | Justiça Estadual Masculino",
		"[Q_4_1.1.1] 4.1. População prisional | Presos sentenciados - regime fechado | Justiça Estadual Feminino",
		"[Q_4_1.2.1] 4.1. População prisional | Presos sentenciados - regime fechado | Justiça Federal Masculino",
		"[Q_4_1.3.1] 4.1. População prisional | Presos sentenciados - regime fechado | Justiça Federal Feminino",
		"[Q_4_1.4.1] 4.1. População prisional | Presos sentenciados - regime fechado | Outros  (Justiça do Trabalho, Cível) Masculino",
		"[Q_4_1.5.1] 4.1. População prisional | Presos sentenciados - regime fechado | Outros  (Justiça do Trabalho, Cível) Feminino",
		"[Q_4_1.0.2] 4.1. População prisional | Presos sentenciados - regime semiaberto | Justiça Estadual Masculino",
		"[Q_4_1.1.2] 4.1. População prisional | Presos sentenciados - regime semiaberto | Justiça Estadual Feminino",
		"[Q_4_1.2.2] 4.1. População prisional | Presos sentenciados - regime semiaberto | Justiça Federal Masculino",
		"[Q_4_1.3.2] 4.1. População prisional | Presos sentenciados - regime semiaberto | Justiça Federal Feminino",
		"[Q_4_1.4.2] 4.1. População prisional | Presos sentenciados - regime semiaberto | Outros  (Justiça do Trabalho, Cível) Masculino",
		"[Q_4_1.5.2] 4.1. População prisional | Presos sentenciados - regime semiaberto | Outros  (Justiça do Trabalho, Cível) Feminino",
		"[Q_4_1.0.3] 4.1. População prisional | Presos sentenciados - regime aberto | Justiça Estadual Masculino",
		"[Q_4_1.1.3] 4.1. População prisional | Presos sentenciados - regime aberto | Justiça Estadual Feminino",
		"[Q_4_1.2.3] 4.1. População prisional | Presos sentenciados - regime aberto | Justiça Federal Masculino",
		"[Q_4_1.3.3] 4.1. População prisional | Presos sentenciados - regime aberto | Justiça Federal Feminino",
		"[Q_4_1.4.3] 4.1. População prisional | Presos sentenciados - regime aberto | Outros  (Justiça do Trabalho, Cível) Masculino",
		"[Q_4_1.5.3] 4.1. População prisional | Presos sentenciados - regime aberto | Outros  (Justiça do Trabalho, Cível) Feminino",
		"[Q_4_1.0.4] 4.1. População prisional | Medida de segurança - internação | Justiça Estadual Masculino",
		"[Q_4_1.1.4] 4.1. População prisional | Medida de segurança - internação | Justiça Estadual Feminino",
		"[Q_4_1.2.4] 4.1. População prisional | Medida de segurança - internação | Justiça Federal Masculino",
		"[Q_4_1.3.4] 4.1. População prisional | Medida de segurança - internação | Justiça Federal Feminino",
		"[Q_4_1.4.4] 4.1. População prisional | Medida de segurança - internação | Outros  (Justiça do Trabalho, Cível) Masculino",
		"[Q_4_1.5.4] 4.1. População prisional | Medida de segurança - internação | Outros  (Justiça do Trabalho, Cível) Feminino",
		"[Q_4_1.0.5] 4.1. População prisional | Medida de segurança - tratamento ambulatorial | Justiça Estadual Masculino",
		"[Q_4_1.1.5] 4.1. População prisional | Medida de segurança - tratamento ambulatorial | Justiça Estadual Feminino",
		"[Q_4_1.2.5] 4.1. População prisional | Medida de segurança - tratamento ambulatorial | Justiça Federal Masculino",
		"[Q_4_1.3.5] 4.1. População prisional | Medida de segurança - tratamento ambulatorial | Justiça Federal Feminino",
		"[Q_4_1.4.5] 4.1. População prisional | Medida de segurança - tratamento ambulatorial | Outros  (Justiça do Trabalho, Cível) Masculino",
		"[Q_4_1.5.5] 4.1. População prisional | Medida de segurança - tratamento ambulatorial | Outros  (Justiça do Trabalho, Cível) Feminino"
	]

	header_escolaridade = [
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Analfabeto | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Analfabeto | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Alfabetizado (sem cursos regulares) | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Alfabetizado (sem cursos regulares) | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Fundamental Incompleto | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Fundamental Incompleto | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Fundamental Completo | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Fundamental Completo | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Médio Incompleto | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Médio Incompleto | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Médio Completo | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Médio Completo | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Superior Incompleto | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Superior Incompleto | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Superior Completo | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino Superior Completo | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino acima de Superior Completo | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Ensino acima de Superior Completo | Feminino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Não Informado | Masculino",
		"5.5.a. Em caso positivo, total ou parcialmente, preencha as informações abaixo | Não Informado | Feminino"
	]

	

	for each in reader:
		row = {}
		for i, field in enumerate(header):
			row[new_header[i]] = each[field]
		
		populacao_mas = []
		populacao_fem = []
		populacao_total = 0
		for field in header_populacao:
			populacao = {}
			if field.find("provisórios") > 0:
				populacao["tipo"] = "provisorios"
			elif field.find("sentenciados") > 0:
				populacao["tipo"] = "sentenciados"
			else:
				populacao["tipo"] = "medida de sentenciadosegurança"

			if field.find("Estadual") > 0:
				populacao["justica"] = "estadual"
			elif field.find("Federal") > 0:
				populacao["justica"] = "federal"
			else:
				populacao["justica"] = "outros"

			populacao["total"] = each[field]

			try:
				if isnumber(each[field]):
					populacao_total += each[field]
					print(populacao_total)
			except Exception as e:
				populacao_total += 0

			if field.find("Masculino") > 0:
				populacao_mas.append(populacao)
			else:
				populacao_fem.append(populacao)

		escolaridade_mas = []
		escolaridade_fem = []
		for field in header_escolaridade:
			escolaridade = {}
			if field.find("Analfabeto") > 0:
				escolaridade["nivel"] = "analfabeto"
			elif field.find("Alfabetizado") > 0:
				escolaridade["nivel"] = "alfabetizado"
			elif field.find("Fundamental") > 0:
				escolaridade["nivel"] = "fundamental"
			elif field.find("Médio") > 0:
				escolaridade["nivel"] = "medio"
			elif field.find("acima de Superior Completo") > 0:
				escolaridade["nivel"] = "pos graduado"
			elif field.find("Superior Incompleto") > 0:
				escolaridade["nivel"] = "superior incompleto"
			elif field.find("Superior Completo") > 0:
				escolaridade["nivel"] = "superior completo"
			else:
				escolaridade["nivel"] = "nao informado"

			escolaridade["total"] = each[field]

			if field.find("Masculino") > 0:
				escolaridade_mas.append(escolaridade)
			else:
				escolaridade_fem.append(escolaridade)

		capacidade = 0
		for field in header_capacidade:
			if each[field] == "":
				continue
			try:
				capacidade += int(each[field])
			except Exception as e:
				capacidade += 0

		row["capacidade"] = capacidade
		row["populacao_total"] = populacao_total

		populacao = {}
		escolaridade = {}

		row["masculino"] = populacao
		row["feminino"] = populacao

		row["masculino"]["populacao"] = populacao_mas
		row["masculino"]["escolaridade"] = escolaridade_mas
		row["feminino"]["populacao"] = populacao_fem
		row["feminino"]["escolaridade"] = escolaridade_fem

		x = row["uf"].find("(") + 1
		y = x + 2
		row["uf"] = row["uf"][x:y]

		#doc = db.penitenciarias.find_one({"unidade": row["unidade"]})
		#print(doc)
		#input()

		db.penitenciarias.insert(row)
	
	# penitenciarias = db.penitenciarias
	# for doc in penitenciarias.find():
	# 	print(doc["unidade"])














	#infopen = pd.read_csv(file_path, delimiter=';', memory_map=True)
	#columns = list(infopen)
	# for c in columns:
	# 	print(c)
	#print(infopen[columns[1300]])
	# print(len(columns))

	# for index, row in infopen.iterrows():
	# 	print(row)
		# input()
