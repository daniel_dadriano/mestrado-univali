import csv
import pymongo

COLUNS = ["sg_uf", "vl_br", "vl_codigo", "vl_km_inic", "vl_km_fina", 
		  "ds_sup_fed", "C_Flow", "D_Flow"]

ROWS = ["uf", "br", "snv", "km_inicial", "km_final", 
		"superficie", "crescente", "decrescente"]

YEARS = [2016]

def import_data(year, db):
	file_path = './Modelagem{0}.csv'.format(year)
	csvfile = open(file_path, "r")
	reader = csv.DictReader(csvfile, delimiter=';')

	for each in reader:
		row = {}
		for i, field in enumerate(COLUNS):
			if field == "vl_br":
				row[ROWS[i]] = int(each[field])
			elif field == "vl_km_inic" or field == "vl_km_fina":
				row[ROWS[i]] = float(each[field])
			elif field == "C_Flow" or field == "D_Flow":
				try:
					row[ROWS[i]] = int(each[field])
				except Exception as e:
					row[ROWS[i]] = 0
			else:
				row[ROWS[i]] = each[field]

		db.vmda.insert(row)


if __name__ == '__main__':
	print("Importanto VMDA")
	mongo_client = pymongo.MongoClient("localhost", 27017)
	db = mongo_client["acidentes"]
	import_data(2016, db)
