import argparse
import numpy
import pandas as pd
import csv
import pymongo
from datetime import datetime


COLUNS = ["data_inversa","dia_semana","horario","uf","br","km","municipio",
		  "causa_acidente","tipo_acidente","classificacao_acidente","fase_dia",
		  "sentido_via","condicao_metereologica","tipo_pista","tracado_via",
		  "uso_solo","pessoas","mortos","feridos_leves","feridos_graves","ilesos",
		  "ignorados","feridos","veiculos"]


YEARS = [2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016]


def import_data(year, db, datetime_format = "%Y-%m-%d"):
	file_path = './{0}.csv'.format(year)

	csvfile = open(file_path, "r")
	reader = csv.DictReader(csvfile, delimiter=';')
	for each in reader:
		row = {}
		for field in COLUNS:
			if field == "data_inversa":
				date = datetime.strptime("{0}".format(each["data_inversa"]), datetime_format)
				row["data"] = date
				#"{0}".format(each["data_inversa"], datetime_format)
			else:
				row[field] = each[field]

		db.acidentes.insert(row)


if __name__ == '__main__':
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--import", required=True, help="import csv?")
	args = vars(ap.parse_args())

	mongo_client = pymongo.MongoClient("localhost", 27017)
	db = mongo_client["acidentes"]

	if args["import"] == "True":
		for year in YEARS:
			print("Import CSV data from {0}".format(year))
			if year == 2016:
				import_data(year, db, datetime_format="%d/%m/%y")
			elif year in [2007, 2008, 2009, 2010, 2011]:
				import_data(year, db, datetime_format="%d/%m/%Y")
			else:
				import_data(year, db)
