import argparse
import numpy
import pandas as pd
import csv
import pymongo
from datetime import datetime


COLUNS = ["id","pesid","data_inversa","dia_semana","horario","uf","br","km",
		  "municipio","causa_acidente","tipo_acidente","classificacao_acidente",
		  "fase_dia","sentido_via","condicao_metereologica","tipo_pista","tracado_via",
		  "uso_solo","id_veiculo","tipo_veiculo","marca","ano_fabricacao_veiculo",
		  "tipo_envolvido","estado_fisico","idade","sexo","nacionalidade","naturalidade"]


# YEARS = [2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016]
YEARS = [2016]


def import_data(year, db, datetime_format = "%Y-%m-%d"):
	file_path = './acidentes{0}.csv'.format(year)

	csvfile = open(file_path, "r")
	reader = csv.DictReader(csvfile, delimiter=';')
	for each in reader:
		row = {}
		for field in COLUNS:
			if field == "data_inversa":
				date = datetime.strptime("{0}".format(each["data_inversa"]), datetime_format)
				row["data"] = date
				#"{0}".format(each["data_inversa"], datetime_format)
			elif field == 'idade':

				age = each[field]
				
				try:
					age = int(age)
				except Exception as e:
					age = -1

				row[field] = age

			elif field == 'br':

				br = each[field]
				try:
					br = int(br)
				except Exception as e:
					br = -1
				row[field] = br

			elif field == 'ano_fabricacao_veiculo':
				year = each[field]

				try:
					year = int(year)
				except Exception as e:
					year = -1

				row[field] = year
			else:
				row[field] = each[field]

		db.vitimas2.insert(row)


if __name__ == '__main__':
	mongo_client = pymongo.MongoClient("localhost", 27017)
	db = mongo_client["acidentes"]

	for year in YEARS:
		print("Import CSV data from {0}".format(year))
		if year == 2016:
			import_data(year, db, datetime_format="%d/%m/%y")
		elif year in [2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015]:
			import_data(year, db, datetime_format="%d/%m/%Y")
		else:
			import_data(year, db)
