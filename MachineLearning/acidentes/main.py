import pandas as pd
import pymongo
from pprint import pprint
from bson.son import SON
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import pearsonr
from sklearn.cluster import KMeans, AffinityPropagation
from sklearn import svm
from datetime import datetime
# import os.path
# import tornado.auth
# import tornado.escape
# import tornado.httpserver
# import tornado.ioloop
# import tornado.options
# import tornado.web
# from tornado.options import define, options

# define("port", default=8000, help="run on the given port", type=int)



COLUNS = ["data","dia_semana","uf","br","km","municipio",
		  "causa_acidente","tipo_acidente","classificacao_acidente","fase_dia",
		  "sentido_via","condicao_metereologica","tipo_pista","tracado_via",
		  "uso_solo","pessoas","mortos","feridos_leves","feridos_graves","ilesos",
		  "ignorados","feridos","veiculos"]

# 1 - Automovel
# 2 - Motocicleta
# 3 - Caminhao
# 4 - Onibus
# 5 - Caminhonete

# 1 Ultrapassagem indevida
# 2 Velocidade incompatível
# 3 Não guardar distância de segurança
# 4 Animais na Pista
# 5 Dormindo
# 6 Outras
# 7 Defeito mecânico em veículo
# 8 Ingestão de álcool
# 9 Defeito na via
# 10 Falta de atenção
# 11 Desobediência à sinalização

# 1 Incêndio
# 2 Colisão com objeto móvel
# 3 Queda de motocicleta / bicicleta / veículo
# 4 Saída de Pista
# 5 Colisão traseira
# 6 Tombamento
# 7 Danos Eventuais
# 8 Atropelamento de animal
# 9 Colisão frontal
# 10 Capotamento
# 11 Derramamento de Carga
# 12 Colisão Transversal
# 13 Colisão lateral
# 14 Colisão com objeto fixo
# 15 (null)
# 16 Colisão com bicicleta
# 17 Atropelamento de pessoa


# class Application(tornado.web.Application):
# 	"""docstring for Application"""
# 	def __init__(self):
# 		handlers = [
# 			(r"/", MainHandler)
# 		]

# 		mongo_client = pymongo.MongoClient("localhost", 27017)		
# 		self.db = mongo_client["acidentes"]
# 		tornado.web.Application.__init__(self, handlers)
		

# class MainHandler(tornado.web.RequestHandler):
# 	def get(self):
# 		pipe = [{
# 			"$group": {"_id": "$estado_fisico", "count": {"$sum": 1}}
# 		}]
# 		victs = list(self.application.db.vitimas.aggregate(pipeline=pipe))
# 		for vic in victs:
# 			self.write(vic)


def dia_semana(db):
	pipe = [
		{"$group": {"_id": "$dia_semana", "count": {"$sum": 1}}},
		{"$sort": SON([("count", 1), ("_id", 1)])}
	]

	acidentes = list(db.acidentes.aggregate(pipeline=pipe))

	print("Quantidade de acidentes por dia da semana")
	pprint(acidentes)

	arr = np.zeros(len(acidentes))
	dias = []
	for i, dia_semana in enumerate(acidentes):
		arr[i] = dia_semana["count"]
		dias.append(dia_semana["_id"])
		acidentes_dia(db, dia_semana["_id"])

	plt.plot(arr)
	plt.title("Quantidade de acidentes por dia da semana")
	plt.ylabel("Acidentes")
	plt.xlabel(dias)
	plt.show()


def acidentes_dia(db, dia_semana):
	pipe = [
		{"$match": {"dia_semana": "{0}".format(dia_semana)}},
		{"$group": {"_id": "$causa_acidente", "count": {"$sum": 1}}},
		{"$sort": {"count": -1}}
	]

	motivos = list(db.acidentes.aggregate(pipeline=pipe))
	print("\n \n Acidentes ocorridos no dia {0}".format(dia_semana))
	pprint(motivos)
		

def rodovias_qtd_acidentes(db):
	date = datetime(2016, 1, 1)
	pipe = [
		{"$match": {"data": {"$gte": date}}},
		{"$group": {"_id": {"uf": "$uf", "br": "$br", "km": "$km", "sentido_via": "$sentido_via", "tipo_pista": "$tipo_pista","tracado_via": "$tracado_via"}, "count": {"$sum": 1}}},
		{"$sort": SON([("count", -1), ("_id", -1)])},
	 	{"$limit": 50}
	]

	rodovias = list(db.acidentes.aggregate(pipeline=pipe))
	rodovias = pd.DataFrame(rodovias)

	acidentes = np.zeros(len(rodovias))
	vmdas = np.zeros(len(rodovias))
	for i, rodovia in rodovias.iterrows():
		vmda = get_vmda(db, rodovia[0]["uf"], rodovia[0]["br"], rodovia[0]["km"])

		if rodovia[0]["sentido_via"] == "Crescente":
			vmda = vmda[0]["crescente"]
		else:
			vmda = vmda[0]["decrescente"]

		acidentes[i] = rodovia[1]
		vmdas[i] = vmda

		print("UF: {0} | BR: {1} | KM: {2} | Sentido: {3} | VMDA: {4} | Tipo Pista: {5} | Tracado: {6} | Total acidentes: {7}".
			format(rodovia[0]["uf"], rodovia[0]["br"], rodovia[0]["km"], rodovia[0]["sentido_via"], vmda, rodovia[0]["tipo_pista"], rodovia[0]["tracado_via"], rodovia[1]))

	plt.figure(1)
	plt.subplot(211)
	plt.title("Quantidade de acidentes")
	plt.plot(acidentes)
	plt.subplot(212)
	plt.title("VMDA")
	plt.plot(vmdas)
	plt.show()

	# 	dados_rodovia(db, rodovia[0]["br"], rodovia[0]["uf"])


def get_vmda(db, uf, br, km):
	try:
		arguments = {"crescente": 1, "decrescente": 1, "snv": 1, "superficie": 1, "_id": 0}
		query = {"uf": uf, "br": int(br), "km_inicial": {"$lte": float(km)}, "km_final": {"$gte": float(km)}}
	except Exception as e:
		km = km.replace(",", ".")
		query = {"uf": uf, "br": int(br), "km_inicial": {"$lte": float(km)}, "km_final": {"$gte": float(km)}}

	cursor = list(db.vmda.find(query, arguments))
	return cursor


def dados_rodovia(db, rodovia, estado):
	pipe = [
		{"$match": {"$and": [{"uf": "{0}".format(estado)}, {"br": "{0}".format(rodovia)}]}},
		{"$group": {"_id": "$km", "count": {"$sum": 1}}},
		{"$sort": {"count": -1}},
		{"$limit": 10}
	]

	kms = list(db.acidentes.aggregate(pipeline=pipe))
	kms = pd.DataFrame(kms)
	for i, km in kms.iterrows():
		print("Km: {0} | Quantidade Acidentes: {1}".format(km[0], km[1]))


def vitimas_sexo(db):
	pipe = [
		{"$match": {"tipo_envolvido" : "Condutor"}},
		{"$group": {"_id": "$sexo", "count": {"$sum": 1}}},
		{"$sort": SON([("count", -1), ("_id", -1)])}
	]
	sexo = list(db.vitimas.aggregate(pipeline=pipe))

	arr = np.zeros(len(sexo))
	leg = []
	for i, s in enumerate(sexo):
		arr[i] = s["count"]
		leg.append(s["_id"])

	y_pos = np.arange(len(leg))


	plt.bar(y_pos, arr, align="center", alpha=0.5)
	plt.xticks(y_pos, leg)
	plt.title("Acidentes por sexo")
	plt.ylabel("Quantidade")
	plt.show()
	pprint(sexo)


def causas_acidentes(db):
	pipe = [
		{"$group": {"_id": "$causa_acidente", "count": {"$sum": 1}}},
		{"$sort": SON([("count", -1), ("_id", -1)])}
	]

	causas = list(db.acidentes.aggregate(pipeline=pipe))
	pprint(causas)


def causa_dia_semana(db):
	pipe = [
		{"$match": {"causa_acidente": "Ingestão de álcool"}},
		{"$group": {"_id": "$dia_semana", "count": {"$sum": 1}}},
		{"$sort": SON([("count", -1), ("_id", -1)])}
	]

	causas = list(db.acidentes.aggregate(pipeline=pipe))
	pprint(causas)


def idade(db):
	pipe = [
		{"$match": {"idade": "126"}},
		{"$group": {"_id": "$idade", "count": {"$sum": 1}}},
		{"$sort": SON([("count", -1), ("_id", -1)])}
	]

	idade = list(db.vitimas.aggregate(pipeline=pipe))
	pprint(idade)


def perfil(db):
	pipe = [
		{"$match": {"$and": [{"sexo": "Masculino"}, {"idade": {"$gte": 18}}, {"idade": {"$lte": 60}}, {"causa_acidente": "Ingestão de álcool"}]}},
		{"$group": {"_id": "$idade", "count": {"$sum": 1}}},
		{"$sort": {"_id": 1}}	
	]

	data = list(db.vitimas.aggregate(pipeline=pipe))

	arr = np.zeros(len(data))
	leg = []
	for i, s in enumerate(data):
		arr[i] = s["count"]
		leg.append(s["_id"])

	y_pos = np.arange(len(leg))

	plt.bar(y_pos, arr, align="center", alpha=0.5)
	plt.xticks(y_pos, leg)
	plt.title("Acidentes por idade")
	plt.ylabel("Quantidade")
	plt.show()
	pprint(data)


def update_tipo_acidente(db):
	tipo_acidentes = ["Incêndio", "Colisão com objeto móvel", 
					 "Queda de motocicleta / bicicleta / veículo", 
					 "Capotamento", "Danos Eventuais", "Atropelamento de animal",
					 "Saída de Pista", "Colisão traseira", "Tombamento", "Colisão frontal",
					 "Derramamento de Carga", "Colisão Transversal", "Colisão lateral",
					 "Colisão com bicicleta", "Atropelamento de pessoa", "Colisão com objeto fixo"]

	for i, tipo_acidente in enumerate(tipo_acidentes):
		db.acidentes.update_many(
			{"tipo_acidente": tipo_acidente},
			{"$set": {"tipo_acidente_n": i + 1}}
		)


def update_fase_dia(db):
	print("Atualizando fases do dia")
	fases_dia = ["Sol", "Neve", "Nublado", "Chuva", "Ceu Claro", 
				 "Nevoeiro/neblina", "Ignorada", "Vento", "Granizo"]

	for i, fase_dia in enumerate(fases_dia):
		print("Fase atual: {0}".format(fase_dia))
		db.acidentes.update_many(
			{"fase_dia": fase_dia},
			{"$set": {"fase_dia_n": i + 1}}
		)


def update_tracado_via(db):
	print("Atualizando tracados da via")
	tracados = ["Cruzamento", "Curva", "Reta"]

	for i, tracado in enumerate(tracados):
		print("Tracado: {0}".format(tracado))
		db.acidentes.update_many(
			{"tracado_via": tracado},
			{"$set": {"tracado_via_n": i + 1}}
		)


def update_classificacao(db):
	print("Atualizando classificacao_acidente")
	classificacoes = ["Ignorado", "Com Vítimas Feridas", "Sem Vítimas", "Com Vítimas Fatais"]
	
	for i, clas in enumerate(classificacoes):
		print("Classificacao: {0}".format(clas))
		db.acidentes.update_many(
			{"classificacao_acidente": clas},
			{"$set": {"classificacao_acidente_n": i + 1}}
		)


def update_tipo_pista(db):
	print("Atualizando tipo_pista")
	tipos = ["Múltipla", "Simples", "Dupla"]

	for i, tipo in enumerate(tipos):
		print("Tipo: {0}".format(tipo))
		db.acidentes.update_many(
			{"tipo_pista": tipo},
			{"$set": {"tipo_pista_n": i + 1}}
		)


def update_causa_acidente(db):
	print("Atualizando causa acidente")
	causas = ["Ultrapassagem indevida", "Defeito mecânico em veículo",
			  "Animais na Pista", "Não guardar distância de segurança",
			  "Velocidade incompatível", "Defeito na via", "Ingestão de álcool",
			  "Dormindo", "Outras", "Falta de atenção", "Desobediência à sinalização"]

	for i, causa in enumerate(causas):
		print("Causa: {0}".format(causa))
		db.acidentes.update_many(
			{"causa_acidente": causa},
			{"$set": {"causa_acidente_n": i + 1}}
		)


def analise(db):
	arguments = {"tipo_acidente_n": {"$exists": "true"}, "fase_dia_n": {"$exists": "true"},
				 "tracado_via_n": {"$exists": "true"}, "classificacao_acidente_n": {"$exists": "true"},
				 "tipo_pista_n": {"$exists": "true"}, "causa_acidente_n": {"$exists": "true"}}

	query = {"tipo_acidente_n": 1, "fase_dia_n": 1, "tracado_via_n": 1,
			 "classificacao_acidente_n": 1, "tipo_pista_n": 1, "causa_acidente_n": 1,
			 "tipo_acidente": 1, "fase_dia": 1, "tracado_via": 1,
			 "classificacao_acidente": 1, "tipo_pista": 1, "causa_acidente": 1}


	cursor = db.acidentes.find(arguments, query).limit(50000)
	acidentes = pd.DataFrame(list(cursor))
	acidentes_bkp = acidentes.copy()
	
	tipo_acidente_n = acidentes["tipo_acidente_n"].max()
	acidentes["tipo_acidente_n"] = acidentes["tipo_acidente_n"] / tipo_acidente_n

	fase_dia_n = acidentes["fase_dia_n"].max()
	acidentes["fase_dia_n"] = acidentes["fase_dia_n"] / fase_dia_n

	tracado_via_n = acidentes["tracado_via_n"].max()
	acidentes["tracado_via_n"] = acidentes["tracado_via_n"] / tracado_via_n

	classificacao = acidentes["classificacao_acidente_n"]
	print("Classificacao 1: {0} | Classificacao Ultimo: {1}".format(classificacao[0], classificacao[len(classificacao) - 1]))
	classificacao_acidente_n = acidentes["classificacao_acidente_n"].max()
	acidentes["classificacao_acidente_n"] = acidentes["classificacao_acidente_n"] / classificacao_acidente_n

	tipo_pista_n = acidentes["tipo_pista_n"].max()
	acidentes["tipo_pista_n"] = acidentes["tipo_pista_n"] / tipo_pista_n

	causa_acidente_n = acidentes["causa_acidente_n"].max()
	acidentes["causa_acidente_n"] = acidentes["causa_acidente_n"] / causa_acidente_n

	tipo_acidente = acidentes["tipo_acidente_n"]
	fase_dia = acidentes["fase_dia_n"]
	tracado_via = acidentes["tracado_via_n"]
	classificacao_acidente = acidentes["classificacao_acidente_n"]
	tipo_pista = acidentes["tipo_pista_n"]
	causa_acidente = acidentes["causa_acidente_n"]

	data_clustering = pd.concat([tipo_acidente, fase_dia, tracado_via, tipo_pista, causa_acidente, classificacao_acidente], axis=1)
	data_classification = pd.concat([tipo_acidente, fase_dia, tracado_via, tipo_pista, causa_acidente], axis=1)

	data_clustering = data_clustering.values
	data_classification = data_classification.values
	classificacao = classificacao.values

	# pprint(data_clustering[len(data_clustering) - 1])

	print("Clustering")
	est = KMeans(n_clusters=4)
	est.fit(data_clustering[:-10])
	labels = est.labels_
	print(labels)

	data_predict = data_clustering[len(data_clustering) - 1]
	data_predict = data_predict.reshape(1, -1)
	predict_cluster = est.predict(data_predict)

	del acidentes_bkp["causa_acidente_n"]
	del acidentes_bkp["classificacao_acidente_n"]
	del acidentes_bkp["fase_dia_n"]
	del acidentes_bkp["tipo_acidente_n"]
	del acidentes_bkp["tipo_pista_n"]
	del acidentes_bkp["tracado_via_n"]

	print(acidentes_bkp.iloc[0])
	print(acidentes_bkp.iloc[len(acidentes_bkp) - 1])
	print("Predict: {0}".format(predict_cluster))

	# for i in range(10, 0, -1):
	# 	data_predict = data_clustering[len(data_clustering) - i]

	# # data_predict = data_clustering[len(data_clustering) - 1]
	# 	data_predict = data_predict.reshape(1, -1)
	# 	predict_cluster = est.predict(data_predict)

	# 	# print_acidente(acidentes_bkp[:1])
	# 	# print_acidente(acidentes_bkp.tail(1))

	# 	print(acidentes_bkp.iloc[len(acidentes_bkp) - i])
	# 	print("Predict: {0}".format(predict_cluster[0]))
	# 	print("\n\n")

		# print(acidentes_bkp.iloc[0])
		# print(acidentes_bkp.iloc[len(acidentes_bkp) - 1])

	# print("=================")

	# print("Classification")
	# clf = svm.SVC()
	# clf.fit(data_classification[:-1], classificacao[:-1])
	# data_predict = data_classification[len(data_classification) - 1]
	# data_predict = data_predict.reshape(1, -1)
	# print("Fit ok...")
	# predict = clf.predict(data_predict)
	# print("Real: {0} | Predict: {1}".format(classificacao[len(classificacao) - 1], predict))


if __name__ == '__main__':

	# tornado.options.parse_command_line()
	# http_server = tornado.httpserver.HTTPServer(Application())
	# http_server.listen(options.port)
	# tornado.ioloop.IOLoop.instance().start()

	mongo_client = pymongo.MongoClient("localhost", 27017)
	db = mongo_client["acidentes"]

	# update_tipo_acidente(db)
	# update_fase_dia(db)
	# update_tracado_via(db)
	# update_classificacao(db)
	# update_tipo_pista(db)
	# update_causa_acidente(db)

	# dia_semana(db)
	# rodovias_qtd_acidentes(db)
	# vitimas_sexo(db)
	# causas_acidentes(db)
	# causa_dia_semana(db)
	# idade(db)
	# perfil(db)

	analise(db)




	# print(classificacao_acidente)

	# print("===============================")
	# print("classificacao_acidente")
	# print("tipo_acidente: {0}".format(pearsonr(classificacao_acidente, tipo_acidente)))
	# print("fase_dia: {0}".format(pearsonr(classificacao_acidente, fase_dia)))
	# print("tracado_via: {0}".format(pearsonr(classificacao_acidente, tracado_via)))
	# print("tipo_pista: {0}".format(pearsonr(classificacao_acidente, tipo_pista)))
	# print("causa_acidente: {0}".format(pearsonr(classificacao_acidente, causa_acidente)))
	# print("===============================")


	# print("===============================")
	# print("tipo_acidente")
	# print("classificacao_acidente: {0}".format(pearsonr(tipo_acidente, classificacao_acidente)))
	# print("fase_dia: {0}".format(pearsonr(tipo_acidente, fase_dia)))
	# print("tracado_via: {0}".format(pearsonr(tipo_acidente, tracado_via)))
	# print("tipo_pista: {0}".format(pearsonr(tipo_acidente, tipo_pista)))
	# print("causa_acidente: {0}".format(pearsonr(tipo_acidente, causa_acidente)))
	# print("===============================")


	# query = {"$and": [{"sexo_n": 1}, {"idade": {"$gte": 18}}, {"idade": {"$lte": 30}}]}
	# cursor = db.vitimas.find(query).limit(50000)
	# vitimas = pd.DataFrame(list(cursor))
	# del vitimas['_id']

	# m_idade = vitimas["idade"].max()
	# vitimas["idade"] = vitimas["idade"] / m_idade

	# m_causa = vitimas["causa_acidente_n"].max()
	# vitimas["causa_acidente_n"] = vitimas["causa_acidente_n"] / m_causa

	# m_veic = vitimas["tipo_veiculo_n"].max()
	# vitimas["tipo_veiculo_n"] = vitimas["tipo_veiculo_n"] / m_veic

	# m_aci = vitimas["tipo_acidente_n"].max()
	# vitimas["tipo_acidente_n"] = vitimas["tipo_acidente_n"] / m_aci

	# m_sex = vitimas["sexo_n"].max()
	# vitimas["sexo_n"] = vitimas["sexo_n"] / m_sex

	# idade = vitimas["idade"].values
	# causa_acidente = vitimas["causa_acidente_n"].values
	# tipo_veiculo = vitimas["tipo_veiculo_n"].values
	# tipo_acidente = vitimas["tipo_acidente_n"].values
	# sexo = vitimas["sexo_n"].values

	# pprint("idade: {0}".format(pearsonr(sexo, idade)))
	# pprint("causa_acidente: {0}".format(pearsonr(sexo, causa_acidente)))
	# pprint("tipo_veiculo: {0}".format(pearsonr(sexo, tipo_veiculo)))
	# pprint("tipo_acidente: {0}".format(pearsonr(sexo, tipo_acidente)))

	# for row in vitimas.iterrows():
	# 	pprint(row)
	# 	input()


# "idade" : 23,
#     "causa_acidente_n" : 10.0,
#     "tipo_veiculo_n" : 1.0,
#     "tipo_acidente_n" : 9.0,
#     "sexo_n" : 1.0